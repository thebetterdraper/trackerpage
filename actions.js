var jq = $.noConflict();
jq(document).ready(function() {
    var url_to_hit = "https://3vkk34k2t1.execute-api.ap-south-1.amazonaws.com/dev/app/sos/location/";


    function getSessionId() {

        // var url = window.location.href;
        var url = "https://altor.tech/track?id=ZUodvQBLuZ5XNej4l81O";
        var session_id = url.split("=")[1];
        return session_id;
    }

    function getTime() {
        var d = new Date();
        var year = d.getFullYear();
        var month = checkIfTwoDigit(d.getMonth());
        var day = checkIfTwoDigit(d.getDate());

        var hours = checkIfTwoDigit(d.getHours());
        var minutes = checkIfTwoDigit(d.getMinutes());
        var seconds = checkIfTwoDigit(d.getSeconds());

        var timestamp = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;

        return timestamp;

    }

    function checkIfTwoDigit(a) {
        var res = a;
        if ((a / 10) <= 0)
            res = "0" + a;


        return res;
    }

    async function apiCall() {

        var sid = getSessionId();
        var ts = getTime();
        var req = {
            session_id: sid,
            timestamp: ts
        };
        try {
            await jq.post(url_to_hit, req, async function(data, status) {
                console.log(data.location);
                alertMaker(data);
                if (data.user_info != null)
                    changeNameNumberPhoto(data.user_info);
                else {
                    jq(".user_name").eq(0).html("Could not fetch");
                    jq("#user_phone").eq(0).html("Could not fetch");
                    jq(".image_circle").eq(0).attr("src", "./NoLoad.png");
                    window.alert("Value error. Try in a while.");
                }
                if (data.location != null)
                    await myMap(data.location, data.user_info.name);
                else
                    window.alert("Value error. Try in a while.");
            })
        } catch (Exception) {
            jq(".user_name").eq(0).html("Could not fetch");
            jq("#user_phone").eq(0).html("Try reloading the page.");
            jq(".alert").eq(0).html("An error occurred.");
            jq(".image_circle").eq(0).attr("src", "./NoLoad.png");
            jq(".image_circle").eq(0).css({
                "border": "0px"
            });

        }

    }

    function alertMaker(data) {

        var status = data.status;
        if (status === "Success") {
            jq(".alert").eq(0).html("Last location displayed.");
            if (data.location != null) {} else {
                jq(".alert").eq(0).html("An error occurred.");
            }
        } else if (status === "Failed") {
            jq(".alert").eq(0).html("An error occurred.");

        } else {
            jq(".alert").eq(0).html("Link has expired.");
        }



    }

    function changeNameNumberPhoto(user_info) {
        var name = user_info.name;
        var src = user_info.image;
        console.log(src + " is src");
        var number = "tel:" + user_info.number;

        if (name.length > 12)
            name = name.substring(0, 12) + "..";
        jq(".user_name").eq(0).html(user_info.name);

        jq("#user_phone").eq(0).html(user_info.number);

        jq("#user_phone").eq(0).attr({
            "href": number
        });

        jq(".image_circle").eq(0).attr("src", src);


    }

    var marker;

    async function myMap(loc, name) {


        console.log(loc);
        var myCenter = new google.maps.LatLng(loc.latitude, loc.longitude);
        var mapCanvas = document.getElementsByClassName("map")[0];
        var mapOptions = { center: myCenter, zoom: 5 };
        var map = new google.maps.Map(mapCanvas, mapOptions);
        marker = new google.maps.Marker({ position: myCenter });
        marker.setMap(map);
        map.setZoom(20);
        map.setCenter(marker.getPosition());

        google.maps.event.addListener(marker, 'click', function() {
            map.setZoom(20);
            //map.setCenter(marker.getPosition());
            map.panTo(marker.getPosition());
        });


        var infoWindow = new google.maps.InfoWindow({
            content: name.split(" ")[0] + "'s last known location."
        });


        google.maps.event.addListener(marker, 'click', function() {
            infoWindow.open(map, marker);
        });

        //GEvent.trigger(marker, 'click');
        google.maps.event.trigger(marker, 'click');
    }

    jq(".image_circle").eq(0).click(function() {
        google.maps.event.trigger(marker, 'click');

    })

    async function loader() {
        await apiCall();
    }

    loader();


    jq(".refresh_img").eq(0).click(function() {
        location.reload();
    });






});